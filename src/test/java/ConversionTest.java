/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import static org.junit.Assert.*;

/**
 *
 * @author Utilisateur
 */
public class ConversionTest {
    
    /**
     * Test of temperatureConversion method, of class Conversion.
     */
    @org.junit.Test
    public void testNOKTemperatureConversion() {
        System.out.println("temperatureConversion - Fahrenheit en Celsius -et inversement");
        //Given
         Conversion instance = new Conversion();
         
        //When
        double t = 80.0d;
        String u = "";
        double resultatReel = instance.temperatureConversion(t, u);
        double resultatInattendu = 176.1d; 
        //Then
        assertNotEquals(resultatInattendu, resultatReel);

    }
    
    @org.junit.Test
    public void testOkTemperatureConversion() {
        System.out.println("temperatureConversion - Fahrenheit en Celsius -et inversement");
        //Given
         Conversion instance = new Conversion();
         
        //When
        double t = 80.0d;
        String u = "";
        double resultatReel = instance.temperatureConversion(t, u);
        double resultatAttendu = 176.0d;
        float ecartAccepte=0.0f;
        
        //Then
        assertEquals(resultatAttendu, resultatReel, ecartAccepte);

    }
    
}
