/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


/**
 *
 * @author Utilisateur
 */
public class Conversion {
    public double temperatureConversion (double temperature, String unite){
        double result;
        if (unite.equals("F"))
            result= (temperature - 32)* (5.0/9.0);
        else
            result= (temperature * (9.0/5.0)) + 32;
        
        return result;
    }
}
